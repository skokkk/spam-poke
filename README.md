TeamSpeak 3 spam poke lua script. Linux and Windows supported.

## Important Note:

 __As of 2018/End 2017 TeamSpeak no longer includes the Lua plugin by default. You have to download it from the `online plugins section` inside your `addons` section inside `tools -> options.`__

 ![Image of Addon Install](https://gitlab.com/skokkk/spam-poke/raw/master/step_1.png)

 ---

 1. After installing the LUA plugin from the steps above, do **one** of the following options:
     * You will need to either fork this project, remove the -masterXXX part and have it's name just as `spam-poke` inside your `plugins/lua_plugin` folder. I will leave you to figure this out if you really want to be lazy.
     * You can also do it manually through copying text and creating files and folders.
        1. Locate your TeamSpeak TS3Client folder. This isn't always inside your installation folder `Program Files/TeamSpeak 3`, it is usually inside your `%appdata% -> Roaming` folder with the name TS3Client. 
         ![Image of Navigation](https://gitlab.com/skokkk/spam-poke/raw/master/step_2.png)
        2. After navigating to the directory shown above and creating that folder, `go into that folder` and `create a file called events.lua`, and `another called init.lua` - make sure they are `.lua and not .lua.txt` - if you are on windows 10, go to `view` at the top of explorer and` tick the "file name extensions" box`.
            * Go to https://gitlab.com/skokkk/spam-poke/blob/master/init.lua and https://gitlab.com/skokkk/spam-poke/blob/master/events.lua - copy the text from each file on gitlab in to the files you created`.
2. Go to your TeamSpeak client (I recommend closing it and opening it if you had it running), go to `Tools > Options > Addons > My Addons` (Not Browse online! *sound familiar?*).
3. Make sure Lua Plugin is `enabled`, then go to `settings for Lua Plugin`.
4. Tick `spam-poke` if it isn't ticked already.
    * I recommend `unticking testmodule`, it's a pain in the ass for everything.
    * `Press ok`
5. Done. `Steps to use below`

## Using the addon:

1. Right click on the user you want to poke.
2. Hover to `Lua Plugin`
3. You will now be shown a list of options. 
    * Poke Client x10 (fast)
    * Poke Client x100 (fast)
    * Poke Client x10 (slow)
    * Poke Client x100 (slow)

The following is very important to note:

* The fast option is literally `a few milliseconds` before it pokes again. If you do not have a rank that bypasses `spam protection`, you will feel spam protection at it's full force. `It's not nice`.

* The slow option __will freeze your TeamSpeak client__. `There's **nothing** I can do about that`, you have the TeamSpeak 3 developers to thank for blocking access to external libraries which would have allowed multithreading, and then stupidly running the lua plugin (actually all plugins) in one thread!